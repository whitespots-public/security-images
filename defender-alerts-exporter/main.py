import os
import requests
from datetime import datetime, timedelta
import json

# Replace with your own values
# from https://learn.microsoft.com/en-us/microsoft-365/security/defender-endpoint/api/api-hello-world?view=o365-worldwide

# Don't blame us for this variables naming, that's how Microsoft calls them
client_id = os.environ.get('APP_CLIENT_ID', '111111')
client_secret = os.environ.get('SECRET_ID_VALUE', '222222')
tenant_id = os.environ.get('TENANT_ID', '3333333')
report_file_name = os.environ.get('REPORT_FILE_NAME', 'defender_report.json')
report_file_path = os.environ.get('REPORT_FILE_PATH', '/data/')
DAYS = int(os.environ.get('DAYS', 30))
token = ''

# Replace with your own values
resource_app_id_uri = 'https://api.securitycenter.microsoft.com'

# Define the OAuth URL
oauth_url = f'https://login.microsoftonline.com/{tenant_id}/oauth2/token'

# Prepare the request body
auth_body = {
    'resource': resource_app_id_uri,
    'client_id': client_id,
    'client_secret': client_secret,
    'grant_type': 'client_credentials'
}

# Make a POST request to acquire the access token
auth_response = requests.post(oauth_url, data=auth_body)

if auth_response.status_code == 200:
    auth_response_data = auth_response.json()
    token = auth_response_data['access_token']
else:
    print(f'Failed to retrieve the access token. Status code: {auth_response.status_code}')


current_time_utc = datetime.utcnow()
some_time_ago = current_time_utc - timedelta(days=DAYS)
formatted_time = some_time_ago.strftime('%Y-%m-%dT%H:%M:%SZ')


# Create the URL with the time filter
url = f"https://api.securitycenter.microsoft.com/api/alerts?$filter=alertCreationTime ge {formatted_time}"

# Set the request headers
headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': f'Bearer {token}'
}

alerts = list()

try:
    # Send the HTTP GET request and get the results
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        alerts = response.json()['value']
        for alert in alerts:
            pass
    else:
        print(f"Failed to retrieve alerts. Status code: {response.status_code}")
except Exception as e:
    print(f"An error occurred: {str(e)}")

#####################################################
#   Now let's put the data in a right formet
#####################################################

portal_findings_list = list()

for alert in alerts:
    data = dict()
    title = alert.get('title', 'New alert with broken title')
    data['name'] = f'[Defender] {title}'

    creation_time = alert.get('alertCreationTime', 'Unparsed date')
    description = f"""
        [{creation_time}] {alert.get('description', 'New alert with broken description')}
        
        category: {alert.get('category', 'Unable to parse')}
        threatFamilyName: {alert.get('threatFamilyName', 'Unable to parse')}
        computerDnsName: {alert.get('computerDnsName', 'Unable to parse')}
        threatName: {alert.get('threatName', 'Unable to parse')}
        mitreTechniques: {alert.get('mitreTechniques', 'Unable to parse')}
        loggedOnUsers: {alert.get('loggedOnUsers', 'Unable to parse')}
        detectionSource: {alert.get('detectionSource', 'Unable to parse')}
        determination: {alert.get('determination', 'Unable to parse')}
        classification: {alert.get('classification', 'Unable to parse')}
        alertCreationTime: {alert.get('alertCreationTime', 'Unable to parse')}
        firstEventTime: {alert.get('firstEventTime', 'Unable to parse')}
        lastEventTime: {alert.get('lastEventTime', 'Unable to parse')}
        evidence: {alert.get('evidence', 'Unable to parse')}
    """
    data['description'] = description

    severity = alert.get('severity', 'Low')
    data['severity'] = severity

    data['vulnerable_url'] = 'https://security.microsoft.com/alerts/' + alert.get('id', '1111111111')
    portal_findings_list.append(data)

# Specify the file path
file_path = report_file_path + report_file_name

# Write the list of dictionaries to the file
with open(file_path, "w") as json_file:
    json.dump(portal_findings_list, json_file, indent=4)

print(f"Data has been written to {file_path}")
