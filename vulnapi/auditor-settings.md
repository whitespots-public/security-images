Name: VulnAPI
Image: registry.gitlab.com/whitespots-public/security-images/vulnapi:v0.8.7
Run command: vulnapi scan curl https://$DOMAIN/ --report-format json --report-file /data/$REPORT_FILE_NAME $ADDITIONAL_CURL_FLAGS_AND_HEADERS