# Phpcs Security Audit

phpcs-security-audit is a set of PHP_CodeSniffer rules that finds vulnerabilities and weaknesses related to security in PHP code.

https://github.com/FloeDesignTechnologies/phpcs-security-audit

https://packagist.org/packages/pheromone/phpcs-security-audit